import Login from '../PageObject/ClassLogin'
import Profile from '../PageObject/ClassProfile'
import Credential from "../PageObject/Credential";
describe('test', () => {

    before(function (){
        Login.open('http://52.212.87.108/full-profile/1064')
        Login.writeLogin(Credential.login);
        Login.writePass(Credential.pass);
        Login.clickLoginButton()
        Login.checkUrl('http://52.212.87.108/profile/1')

    });

    it('Change Avatar', function () {
        Profile.open('http://52.212.87.108/full-profile/1064')
        Profile.uploadAvatar()

    });

    it('Change First Name', function () {
        Profile.open('http://52.212.87.108/full-profile/1064')
        Profile.writeNameInFirstNameRusField()
        Profile.clickOkButton()
        Profile.checkFirstNameRusField()
    });

    it('Change First Name with 256 symbols', function () {
        Profile.open('http://52.212.87.108/full-profile/1064')
        Profile.takeTextFromFirstNameRusField()
        Profile.writeToLongTextToFirstNameRusField()
        Profile.clickOkButton()
        Profile.checkErrorMessageInFirstNameRusField()
        Profile.clickCancelButton()
        Profile.checkTextinFirstNameRusField()
    });

    it('Change First Name with empty field', function () {
        Profile.open('http://52.212.87.108/full-profile/1064')
        Profile.takeTextFromFirstNameRusField()
        Profile.makeFirstNameRusFieldIsEmpty()
        Profile.clickOkButton()
        Profile.checkErrorMessageInFirstNameRusField()
        Profile.clickCancelButton()
        Profile.checkTextinFirstNameRusField()
    });

    it('Update gender', function () {
        Profile.open('http://52.212.87.108/full-profile/1064')
        Profile.changeGender()
        Profile.clickOkButton()
        Profile.changeGender()
    });

    it('Cancel update gender', function () {
        Profile.open('http://52.212.87.108/full-profile/1064')
        Profile.takeGenderText()
        Profile.clickOnGenderWithoutChanging()
        Profile.checkGender()
    });

    it('Choose Permanent schedule', function () {
        Profile.open('http://52.212.87.108/profile/1')
        Profile.openSheduleField()
        Profile.setTimeFromInScheduleField()
        Profile.setTimeTillInScheduleField()
        Profile.changePermanentWorkingTime()
        Profile.checkSuccessMessageInScheduleField()

    });
})