import Login from '../PageObject/ClassLogin'
import Profile from "../PageObject/ClassProfile";
import Credential from "../PageObject/Credential";
import Api from "../PageObject/ApiClass"




describe('test', () => {


    it('first', ()=>{
        Api.loginAndTakeToken()
        Api.changeVocationDays(5)
        Login.open('http://52.212.87.108/full-profile/1064')
        Login.writeLogin(Credential.login);
        Login.writePass(Credential.pass);
        Login.clickLoginButton()
        Login.checkUrl('http://52.212.87.108/profile/1')
        Login.open('http://52.212.87.108/profile/1064')
        Profile.openSheduleField()
        Profile.chooseVacationInFormEventType()
        Profile.typeDateInInputForm(3, 6)
        Profile.writeComment()
        Profile.clickApplyButton()
        Profile.checkVocation()
        Profile.cancelVocation()
    });
    it('first', ()=>{
        Api.loginAndTakeToken()
        Api.changeVocationDays(0)
        Login.open('http://52.212.87.108/profile/1064')
        Login.writeLogin(Credential.login);
        Login.writePass(Credential.pass);
        Login.clickLoginButton()
        Login.checkUrl('http://52.212.87.108/profile/1064')
        Profile.openSheduleField()
        Profile.chooseVacationInFormEventType()
        Profile.typeDateInInputForm(3, 13)
        Profile.writeComment()
        Profile.clickApplyButton()
        Profile.checkErrorMessageInVocationField()



    });



});