import allureReporter from "@wdio/allure-reporter";
import Main from "./MainClass";

class Login extends Main{
    get inputLogin() {return $('[name = "login"]')}
    get inputPass() {return $('[name = "password"]')}
    get loginButton(){return $('[type = "submit"]')}
        writeLogin(loginName) {
            allureReporter.addStep('Write login name')
            this.inputLogin.setValue(loginName)
        }

        writePass(pass) {
            allureReporter.addStep('Write pass')
            this.inputPass.setValue(pass)
        }

        clickLoginButton(){
            allureReporter.addStep('Click login button')
            this.loginButton.click()
        }
    }

export default new Login();









