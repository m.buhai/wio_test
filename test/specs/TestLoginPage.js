import Login from '../PageObject/ClassLogin'
import Credential from "../PageObject/Credential";

describe('test', () => {

    it('Login', () => {
        Login.open('http://52.212.87.108/full-profile/1064')
        Login.writeLogin(Credential.login);
        Login.writePass(Credential.pass);
        Login.clickLoginButton()
        Login.checkUrl('http://52.212.87.108/profile/1')
    });
})