let path = require('path');
import Main from "./MainClass";
import allureReporter from "@wdio/allure-reporter";
class Profile extends Main{
    constructor() {
        super();
        this.textForInput = 'Тест'
        this.checker
        this.checking
        this.pause = '10000'
    }
    get changeAvatarInput() { return $('input[type="file"]')}
    get uploadButtonSuccess(){ return $('[class*="btn-success"]')}
    get inputFirstNameRus() {return $('[type="text"][class*="editable"]')}
    get okButtonFirstNameRus() {return $('[class*=\'glyphicon-ok\']')}
    get cancelButton(){return $('[title="Cancel"]')}
    get genderSelect() {return $('[class*="editable-has-buttons"]')}
    get iconEditWorkSchedule() {return $('[ng-if="$ctrl.isWorkScheduleManaged"]').$('[class*="icon-arrow-down"]')}
    get timeFrom() {return $('#permanentWorkingHoursFrom')}
    get timeTill() {return $('[ng-options*="end"]')}

    uploadAvatar(){
        allureReporter.addStep('Upload the Avatar')
        browser.waitUntil(function (){
            return $('[ng-model="$ctrl.avatar"]').getText() != ""
        }, this.pause);
        browser.execute(function () {
            document.querySelector('label[style*="visibility: hidden"]').style = ' ';
        });

        this.changeAvatarInput.waitForDisplayed();
        let filePath = path.join( './testData/image/newAvatar.png');
        let remoteFilePath = browser.uploadFile(filePath)
        this.changeAvatarInput.setValue(remoteFilePath)
        this.uploadButtonSuccess.click()
        browser.waitUntil(function (){
            return $('[ng-model="$ctrl.avatar"]').getText() != ""
        }, this.pause);
    }
    writeNameInFirstNameRusField(){
        allureReporter.addStep('Write first name')
        let buttonFirstNameRus =  $('button[editable-text="$ctrl.table.firstName.fieldValue"]');
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, this.pause);
        let checker;
        checker = buttonFirstNameRus.getText();
        if (checker === 'Тест'){
            this.textForInput = 'Тестище'
        }
        buttonFirstNameRus.click()

        this.inputFirstNameRus.setValue(this.textForInput)

    }
    checkFirstNameRusField(){
        allureReporter.addStep('Check first name')
        let buttonFirstNameRus =  $('button[editable-text="$ctrl.table.firstName.fieldValue"]');
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, this.pause);
        expect(buttonFirstNameRus.getText()).toEqual(this.textForInput)
    }
    takeTextFromFirstNameRusField(){
        allureReporter.addStep('Take text from first name field')
        let buttonFirstNameRus =  $('button[editable-text="$ctrl.table.firstName.fieldValue"]');
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, this.pause);
        this.checker = buttonFirstNameRus.getText()
    }
    writeToLongTextToFirstNameRusField(){
        allureReporter.addStep('Write text longer then 255 symbols')
        let buttonFirstNameRus =  $('button[editable-text="$ctrl.table.firstName.fieldValue"]');
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, this.pause);

        buttonFirstNameRus.click()

        this.inputFirstNameRus.setValue('TV3DMms22uVqufK6UAu376kC7YpM9bnKIkwas4OzBbRpksziVLhJ392CBye6LDfAeNcwLmtJzW8LxRSnYcVvwJFMs2AtUZB0R5zDe81l7uvhfuQ9R6WlAhn12kvSt3tvrKkS4IJs2DC1PIqsjMqx1yBFKNwrKrj11tqhxVeoC3FsBpMr1BHxso26UUzlXx5YSpMbTViKbBEhhdgru4W2en8unIVJmFbPCiBv8FtBYirX8G8IsAFp2AGP0tGUSBgU')

        this.okButtonFirstNameRus.click()
    }
    checkErrorMessageInFirstNameRusField(){
        allureReporter.addStep('Check error message')
        let errorMessageField = $('[class*="editable-error"]')
        browser.waitUntil(function (){
            return errorMessageField.getText() != ""
        }, this.pause);
        expect(errorMessageField.getText()).toEqual('Text length should be from 1 to 255 symbols. Symbol "-" and space are allowed. Only Russian symbols allowed.')
    }
    clickCancelButton(){
        allureReporter.addStep('Click cancel button')
        this.cancelButton.click()
    }
    checkTextinFirstNameRusField(){
        allureReporter.addStep('Check text in first name rus field')
        let buttonFirstNameRus =  $('button[editable-text="$ctrl.table.firstName.fieldValue"]');
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, this.pause);
        expect(buttonFirstNameRus.getText()).toEqual(this.checker)
    }
    makeFirstNameRusFieldIsEmpty(){
        allureReporter.addStep('Make first name rus field empty')
        let buttonFirstNameRus =  $('button[editable-text="$ctrl.table.firstName.fieldValue"]');
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, this.pause);
        buttonFirstNameRus.click()

        this.inputFirstNameRus.setValue('')
    }
    changeGender(){
        allureReporter.addStep('Change gender')
        let genderButton = $('[editable-select="$ctrl.table.gender.fieldValue"]')
        browser.waitUntil(function (){
            return genderButton.getText() != ""
        }, this.pause);
        this.checking = genderButton.getText()
        genderButton.click()
        this.genderSelect.click()
        let choosingGender = $('[label="Male"]')
        if (this.checking === 'Male'){
            choosingGender = $('[label="Female"]')
        }
        this.checking = choosingGender.getText()
        choosingGender.click()
    }
    checkGender(){
        allureReporter.addStep('Check gender')
        let genderButton = $('[editable-select="$ctrl.table.gender.fieldValue"]')
        browser.waitUntil(function (){

            return genderButton.getText() != ""
        }, this.pause);
        expect(genderButton.getText()).toEqual(this.checking)
    }
    takeGenderText(){
        allureReporter.addStep('Take text from gender field')
        let genderButton = $('[editable-select="$ctrl.table.gender.fieldValue"]')
        browser.waitUntil(function (){
            return genderButton.getText() != ""
        }, this.pause);
        this.checking = genderButton.getText()
    }
    clickOnGenderWithoutChanging(){
        allureReporter.addStep('Click ok without changing gender field ')
        let genderButton = $('[editable-select="$ctrl.table.gender.fieldValue"]')
        genderButton.click()
        this.cancelButton.click()
    }
    clickOkButton(){
        allureReporter.addStep('Click ok button')
        this.okButtonFirstNameRus.click()
    }
    openSheduleField(){
        allureReporter.addStep('Open schedule field')


        this.iconEditWorkSchedule.waitForDisplayed()
        this.iconEditWorkSchedule.click()
    }
    setTimeFromInScheduleField(){
        allureReporter.addStep('Set time in from field')

        this.timeFrom.scrollIntoView()
        this.timeFrom.selectByAttribute('label', '07:00')
    }
    setTimeTillInScheduleField(){
        allureReporter.addStep('Set time in till field')
        this.timeTill.selectByAttribute('label', '10:00')
    }
    changePermanentWorkingTime(){
        allureReporter.addStep('Change permanent working time')
        let changePermanentWorkingTime = $('[ng-click="$ctrl.changePermanentWorkingTime()"]')
        changePermanentWorkingTime.waitForClickable()
        changePermanentWorkingTime.click()
    }
    checkSuccessMessageInScheduleField(){
        allureReporter.addStep('Check success message')
        let successMessageShow = $('[ng-if="$ctrl.data.successMessageShow"]')
        browser.waitUntil(function (){
            return successMessageShow.getText() != ""
        }, this.pause);
        expect(successMessageShow.getText()).toEqual('×'+ '\n' +'Time was successfully changed.')
    }
    chooseVacationInFormEventType(){
        let formEventType = $('[class*="form-control-event-type"]')
        formEventType.selectByAttribute('label', 'Vacation')
    }
    typeDateInInputForm(start, finish){
        let now = new Date();
        let dayFrom = now.getDate()+start;
        let monthFrom = now.getMonth()
        let yearFrom = now.getFullYear()
        let futureFrom = new Date(yearFrom, monthFrom, dayFrom);
        dayFrom = futureFrom.getDate()
        monthFrom = futureFrom.getMonth()+1
        yearFrom = futureFrom.getFullYear()
        if(dayFrom<10){
            dayFrom='0'+String(dayFrom)
        }else {
            dayFrom=String(dayFrom)
        };
        if(monthFrom<10){
            monthFrom='0'+String(monthFrom)
        }else {
            monthFrom=String(monthFrom)
        };
        let inputDateFrom = $('#startDatepickerDatePopUpFrom')
        inputDateFrom.waitForClickable()
        inputDateFrom.click()
        inputDateFrom.setValue(dayFrom+'-'+monthFrom+'-'+String(yearFrom))

        let dayTo = now.getDate()+finish;
        let monthTo = now.getMonth()
        let yearTo = now.getFullYear()
        let futureTo = new Date(yearTo, monthTo, dayTo);
        dayTo = futureTo.getDate()
        monthTo = futureTo.getMonth()+1
        yearTo = futureTo.getFullYear()
        if(dayTo<10){
            dayTo='0'+String(dayTo)
        }else {
            dayTo=String(dayTo)
        };
        if(monthTo<10){
            monthTo='0'+String(monthTo)
        }else {
            monthTo=String(monthTo)
        };
        let inputDateTo = $('#startDatepickerDatePopUpTo')
        inputDateTo.waitForClickable()
        inputDateTo.click()
        inputDateTo.setValue(dayTo+'-'+monthTo+'-'+String(yearTo))
    }
    writeComment(){
        let commentField = $('#editScheduleComment')
        commentField.setValue('Test')
    }
    clickApplyButton(){
        let secondApplyButton = $('//div[5]/button')
        secondApplyButton.click()
    }
    checkVocation(){
        browser.waitUntil(function (){
            return $('[class*="tiles-schedule"]').getText() != ""
        }, 10000);
    }
    cancelVocation(){
        let cancelVocation = $('//div[2]/div/button/span')
        cancelVocation.click()
    }
    checkErrorMessageInVocationField(){
        browser.waitUntil(function (){
            return $('[ng-if="$ctrl.data.errorMessage"]').getText() != ""
        }, 10000);
    }


}
export default new Profile();