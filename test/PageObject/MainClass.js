import allureReporter from "@wdio/allure-reporter";

export default  class Main{

    open(url){
        browser.url(url)
        allureReporter.addStep('Enter the page')
    }
    checkUrl(url){
        expect(browser).toHaveUrlContaining(url)
        allureReporter.addStep('Check url')
    }
}

