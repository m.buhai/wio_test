const fetch = require("node-fetch");
class Api {
    constructor(){
        this.token
    }

    loginAndTakeToken(){

        async function getResponse() {
            let responce = await fetch('http://52.212.87.108/api/auth/login', {
                method: 'POST',
                body: JSON.stringify({"email": "manager@mobidev.biz", "password": "mobidevQA01!"}),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8', 'Accept': 'application/json, text/plain, */*'
                }
            });
            let answer = await responce.text();
            answer = JSON.parse(answer);
            this.token = answer.token;
        }
        getResponse()
    }
    changeVocationDays(days){
        async function getResponse() { let responce = await fetch('http://52.212.87.108/api/vacation-statistic/edit-current-year-vacation', {
                method: 'POST',
                body: JSON.stringify({"UserId":1064,"count":days}),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8', 'Accept': 'application/json, text/plain, */*', 'authorization': 'Bearer ' + this.token
                }
            });
            expect(responce.ok).toEqual(true)
        }
        getResponse();
    }
}
export default new Api();